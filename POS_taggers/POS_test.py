import spacy
from cogroo_interface import Cogroo
cogroo = Cogroo.Instance()


def part_of_speech_spacy(phrase, results):
    nlp = spacy.load('pt_core_news_sm')
    doc = nlp(phrase)
    print('\n' + '-'*20)
    print('Phrase is: ' + doc.text + '\n')
    for token in doc:
        print(token.text, token.pos_, token.dep_)
        results.write(token.text + ' ' + token.pos_ + ' ' + token.dep_ + '\n')


def part_of_speech_cogroo(text):
    '''
    doc = cogroo.analyze(phrase)
    print('\n' + '-' * 20)
    print('Phrase is: ' + phrase + '\n')
    for chunk in doc.sentences[0].chunks:
        for token in chunk.tokens:
            print(' ' + token.pos)
    '''
    noun_chunks_list = []
    pos_tagged_text = cogroo.analyze(text)
    print("-" * 20)
    print(pos_tagged_text.sentences[0].chunks)
    for chunk in pos_tagged_text.sentences[0].chunks:
        chunk_text = text[chunk.start:chunk.end]
        chunk_start = chunk.start
        chunk_end = chunk.end
        #print(chunk_text)
        #print(str(chunk_start) + "-" + str(chunk_end))
        #print("tag: " + str(chunk.tag) + "  tokens:" + str(chunk.tokens))
        if chunk.tag == "NP":
            for token in chunk.tokens:
                if token.pos == "conj-c":
                    if chunk.end == token.end:
                        chunk_text = chunk_text[:token.start - chunk_start - 1]
                        chunk_end = token.start - 1
                    else:
                        chunks = chunk_text.split(" e ")
                        #print(chunks)
                        chunk_text = chunks[0]
                        chunk_text2 = chunks[1]
                        noun_chunks_list.append(chunk_text)
                        chunk_end = token.start - 1

                if token.pos == "art":
                    if chunk.start == token.start:
                        chunk_text = chunk_text[token.end - chunk_start + 1:]
                        chunk_start = token.end + 1
            # if chunk.tag == "VP":

            print("Esse é o chunk: " + str(chunk_text))
            # noun_chunk = ec.Entity(text = chunk_text, start = chunk_start, end = chunk_end)
            # noun_chunks_list.append(noun_chunk)

        #print(noun_chunks_list)


def main():
    phrases = open("frases.txt", "r")
    results = open("results.txt", "w+")
    for line in phrases:
        #part_of_speech_spacy(line, results)
        part_of_speech_cogroo(line)
    phrases.close()


if __name__ == '__main__':
    main()
