import json


def ascii_encode_dict(data):
    ascii_encode = lambda x: x.encode('ascii')
    return dict(map(ascii_encode, pair) for pair in data.items())

config = {'key1': 'reunião', 'key2': 'value2'}

with open('config.json', 'w') as f:
    print(json.dumps(config, indent=1, ensure_ascii=False))
