from __future__ import print_function
import json
from watson_developer_cloud import AssistantV1

# If service instance provides API key authentication
# assistant = AssistantV1(
#     version='2018-07-10',
#     ## url is optional, and defaults to the URL below. Use the correct URL for your region.
#     url='https://gateway.watsonplatform.net/assistant/api',
#     iam_apikey='iam_apikey')

assistant = AssistantV1(
    username='apikey',
    password='423ehYFN--SbgEcuefvrF-o0j9AakZfwHEzTntpuuxhx',
    ## url is optional, and defaults to the URL below. Use the correct URL for your region.
    url='https://gateway.watsonplatform.net/assistant/api',
    version='2018-09-20')

#########################
# Workspaces
#########################

workspace_id = '338785a0-b933-4641-9b12-6a5f1018ac4a'
print('Workspace id {0}'.format(workspace_id))

response = assistant.get_workspace(
    workspace_id=workspace_id, export=True).get_result()
#print(json.dumps(response, indent=2))

#  message
response = assistant.message(
    workspace_id=workspace_id,
    input={
        'text': 'Marcar uma reunião de trabalho'
    }).get_result()
print(json.dumps(response, indent=2))
print(response['intents'][0]['intent'])
print(response['entities'][0]['value'])


