class Event(object):

    def __init__(self, data):
        self.data = data
        self.labels = []

    def is_true(self):
        pass

    def MsgArrived(self):
        if self.data is not None:
            return 'msg_arrived'
        return

    def WhatDetected(self):
        for item in self.labels:
            if item in self.data:
                return 'what_detected'
        return

    def WhitDetected(self):
        for item in self.labels:
            if item in self.data:
                return 'whit_detected'
        return

    def WhenDetected(self):
        for item in self.labels:
            if item in self.data:
                return 'when_detected'
        return

    def WhereDetected(self):
        for item in self.labels:
            if item in self.data:
                return 'where_detected'
        return

    def Response(self):
        return 'response'
