from State import State


# Start of our states
class Idle(State):
    """
    Estado inicial
    """

    def on_event(self, event):
        if event == 'msg_arrived':
            return get_WHAT()

        return self


class get_WHAT(State):
    """
    Estado para procurar with_list
    """

    def on_event(self, event):
        if event == 'what_detected':
            return get_WHIT_LIST()

        return self


class get_WHIT_LIST(State):
    """
    Estado para procurar with_list
    """

    def on_event(self, event):
        if event == 'with_detected':
            return get_WHEN()

        return self


class get_WHEN(State):
    """
    Estado para procurar with_list
    """

    def on_event(self, event):
        if event == 'when_detected':
            return get_WHERE()

        return self


class get_WHERE(State):
    """
    Estado para procurar with_list
    """

    def on_event(self, event):
        if event == 'where_detected':
            return info_management('complete')

        return self


class info_management(State):
    """
    Estado para procurar with_list
    """

    def on_event(self, event):
        if event == 'response' and self.completion == 'complete':
            return Idle()
        if event == 'response' and self.completion != 'complete':
            return Idle()
        return self


# End of our states.