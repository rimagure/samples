from SPARQLWrapper import SPARQLWrapper, N3, JSON, XML
from rdflib import Graph, URIRef

'''
#SPARQLWrapper (SPARQL: semantic query language for databases
g = Graph()
g.parse("http://dbpedia.org/resource/Albert_Einstein")

for stmt in g.subject_objects(URIRef("http://dbpedia.org/ontology/birthDate")):
    print("the person represented by", str(stmt[0]), "was born on", str(stmt[1]))

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?label
    WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
""")

print('\n\n*** JSON Example')
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
print(results['results'])
for result in results["results"]["bindings"]:
    print(result["label"]["value"])

'''

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?label
    WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
""")
print(sparql)
sparql.setReturnFormat(JSON)
print(sparql)
results = sparql.query().convert()
print(results)
for result in results["results"]["bindings"]:
    print(result["label"]["value"])

sparql.setQuery("""
    ASK WHERE { 
        <http://dbpedia.org/resource/Organisation> dbo:country "Spain"
    }    
""")

sparql.setReturnFormat(XML)
results = sparql.query().convert()
print(results.toxml())


'''
#rdf: semantic model for data interchange
sparql.setReturnFormat(N3)
results = sparql.query().convert()
g = Graph()
g.parse(data=results, format="n3")
print(g.serialize(format='n3'))
'''