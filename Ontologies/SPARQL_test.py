import rdflib
from rdflib.namespace import RDF
import json


g = rdflib.Graph()
g.parse("wn20full/wordnet-wordsensesandwords.rdf")
#q1 = "select ?comment where { wn20schema:gloss rdfs:subPropertyOf ?comment}"
q1 = "select ?instances where { ?instances wn20schema:lexicalForm 'registration'}"

y = g.query(q1)
print(y)
z = y.serialize(format="json")
print(z)
my_json = z.decode('utf8').replace("'", '"')
print(my_json)
data = json.loads(my_json)

for result in data["results"]["bindings"]:
    print(result["instances"]["value"])
    n = result["instances"]["value"]

#print("\n")
#q = "select ?Object where {{ <{nome}> rdf:type ?Object}}".format(nome=n)
#print(q)
#q = "select ?y where {?Pessoa2 :Nome ?y}"
#q1 = "select ?Class where { ?Class rdf:type owl:NamedIndividual}"
#q2 = "select ?Object where {{ test:{0!s} rdf:type ?Object}}".format("ricardo")
#q2 = "select ?Object where { test:Ricardo rdf:type ?Object}"
#x = g.query(q)
#z = g.query(q2)
#print(list(x))
#print("\n")
#print(list(z))

#for s,p,o in g.triples((None,  RDF.type, None)):
#    print("%s is %s of %s"%(s,p,o))