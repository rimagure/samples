'''
import spacy


def get_noun_chunks(input_text):
    nlp = spacy.load('pt_core_news_sm')
    doc = nlp(input_text)
    noun_chunks_list = []
    for token in doc:
        print(token.text, token.pos_, token.dep_)
    print('\n')


def main():
    file = open("nomes.txt")
    for text in file:
        get_noun_chunks(text)

main()
'''

import spacy


def get_noun_chunks(input_text):
    nlp = spacy.load('pt_core_news_sm')
    doc = nlp(input_text)
    noun_chunks_list = []
    for ent in doc.ents:
        print('Entidades: ')
        print(ent.text, ent.start_char, ent.end_char, ent.label)
    print('\n')
    for chunk in doc.noun_chunks:
        print('noun_chunks: ')
        print(chunk.text, chunk.root.text, chunk.root.dep_, chunk.root.head.text)

    return noun_chunks_list


def main():
    #input_text = 'quero marcar uma conversa com o Mateus no restaurante hoje'
    #get_noun_chunks(input_text)

    nlp = spacy.load('en_core_web_sm')
    doc = nlp(u'Mr. Best flew to New York on Saturday morning.')
    for entity in doc.ents:
        print(entity.label)
        print(entity.label_)
        print(entity.text)


main()
