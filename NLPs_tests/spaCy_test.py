from NLPs_tests import entity_class_test as ec
import spacy


def get_noun_chunks(input_text):
    nlp = spacy.load('pt_core_news_sm')
    doc = nlp(input_text)
    noun_chunks_list = []
    for token in doc:
        print(token.text, token.pos_, token.dep_)
    print('\n')
    for token in doc:
        if token.pos_ == 'NOUN':
            noun_chunk = ec.Entity(text=token.text, start=token.idx, end=(token.idx+len(token.text)))
            print('Token start ='  + str(token.idx) + 'Token end =' + str(token.idx+len(token.text)))
            print('Token ent: ' + token.ent_type_)
            noun_chunks_list.append(noun_chunk)
        if token.pos_ == 'PROPN' and (token.dep_ == 'nmod' or token.dep_ == 'flat:name'):
            noun_chunk = ec.Entity(text=token.text, start=token.idx, end=(token.idx+len(token.text)))
            print('Token start =' + str(token.idx) + 'Token end =' + str(token.idx + len(token.text)))
            print('Token ent: ' + token.ent_type_)
            noun_chunks_list.append(noun_chunk)

    for entities in noun_chunks_list:
        print(entities.text)

    return noun_chunks_list


def main():
    input_text = 'quero marcar uma conversa com a Maria Prado'
    get_noun_chunks(input_text)

main()

'''
from __future__ import unicode_literals, print_function
#!/usr/bin/env python
# coding: utf8
"""A simple example of extracting relations between phrases and entities using
spaCy's named entity recognizer and the dependency parse. Here, we extract
money and currency values (entities labelled as MONEY) and then check the
dependency tree to find the noun phrase they are referring to – for example:
$9.4 million --> Net income.
Compatible with: spaCy v2.0.0+
"""


import plac
import spacy


TEXTS = [
    'Net income was $9.4 million compared to the prior year of $2.7 million.',
    'Revenue exceeded twelve billion dollars, with a loss of $1b.',
]


@plac.annotations(
    model=("Model to load (needs parser and NER)", "positional", None, str))
def main(model='en_core_web_sm'):
    nlp = spacy.load(model)
    print("Loaded model '%s'" % model)
    print("Processing %d texts" % len(TEXTS))

    for text in TEXTS:
        doc = nlp(text)
        relations = extract_currency_relations(doc)
        for r1, r2 in relations:
            print('{:<10}\t{}\t{}'.format(r1.text, r2.ent_type_, r2.text))


def extract_currency_relations(doc):
    # merge entities and noun chunks into one token
    spans = list(doc.ents) + list(doc.noun_chunks)
    for span in spans:
        span.merge()

    relations = []
    for money in filter(lambda w: w.ent_type_ == 'MONEY', doc):
        if money.dep_ in ('attr', 'dobj'):
            subject = [w for w in money.head.lefts if w.dep_ == 'nsubj']
            if subject:
                subject = subject[0]
                relations.append((subject, money))
        elif money.dep_ == 'pobj' and money.head.dep_ == 'prep':
            relations.append((money.head.head, money))
    return relations


if __name__ == '__main__':
    plac.call(main)
'''

'''
import spacy

nlp = spacy.load('en_core_web_sm')
doc = nlp('I am going to give it a try')
for np in doc.noun_chunks:
    print(np.text)
'''