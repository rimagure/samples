from cogroo_interface import Cogroo


def cogroo_evaluation(file):
    cogroo = Cogroo.Instance()
    acertos = 0
    erros = 0
    while 1:
        line = file.readline()
        if line:
            pos_tagged_text = cogroo.analyze(line.strip()).sentences[0]
            print("")
            print("-"*20)
            print(line)
            print(pos_tagged_text.chunks)
            list1 = []
            line2 = file.readline()
            print(line2.split())
            for chunk in pos_tagged_text.chunks:
                for token in chunk.tokens:
                    list1.append(token.pos)
            for pos, word in zip(list1, line2.split()):
                if pos == word:
                    #print("[Correto]", end=" ")
                    acertos += 1
                else:
                    print("[Errado]", end=" ")
                    erros += 1
                    print("Foi: ", pos, end=" ")
                    print(" | É para ser: ", word)
        else:
            break
        file.readline()
    print("-"*20)
    print("A taxa de acerto foi de: ", (100*acertos/(acertos+erros)), "%")
    print("-"*20)


if __name__ == '__main__':
    phrases = open("/home/ricardo/Documents/Samples/Evaluations/frases_cogroo.txt", mode="r") #trabalho
    # phrases = open("/home/ricardo/Documents/Projects/samples/Evaluations/frases_cogroo.txt", mode="r") #casa
    cogroo_evaluation(phrases)
