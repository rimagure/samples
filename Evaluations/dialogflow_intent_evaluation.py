#!/usr/bin/env python

# Copyright 2017 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""DialogFlow API Detect Intent Python sample with text inputs.
Examples:
  python detect_intent_texts.py -h
  python detect_intent_texts.py --project-id PROJECT_ID \
  --session-id SESSION_ID \
  "hello" "book a meeting room" "Mountain View"
  python detect_intent_texts.py --project-id PROJECT_ID \
  --session-id SESSION_ID \
  "tomorrow" "10 AM" "2 hours" "10 people" "A" "yes"
"""

import os
import dialogflow_v2 as dialogflow

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/ricardo/Documents/Samples/tcc-mecatronica-d5640f1bdf8d.json"


# [START dialogflow_detect_intent_text]
def detect_intent_texts(project_id, session_id, text, language_code):
    """
    Retorna o intent identificado para uma frase dada
    :param project_id:
    :param session_id:
    :param text:
    :param language_code:
    :return: string
    """
    session_client = dialogflow.SessionsClient()

    session = session_client.session_path(project_id, session_id)

    text_input = dialogflow.types.TextInput(
        text=text, language_code=language_code)

    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent(
        session=session, query_input=query_input)

    return response.query_result.intent.display_name
# [END dialogflow_detect_intent_text]


def dialogflow_evaluation(file):
    acertos = 0
    erros = 0
    while 1:
        phrase = file.readline()
        phrase = phrase.strip()
        intent = file.readline()
        intent = intent.strip()
        if phrase:
            detected_intent = detect_intent_texts(
                'tcc-mecatronica', 36, phrase, 'pt-br')
            if intent == detected_intent:
                acertos += 1
            else:
                erros += 1
                print("-" * 20)
                print("[Erro: %s]" %intent)
                print("A frase foi: ", phrase)
                print("Era para ser: ", intent)
                print("Mas foi: ", detected_intent)
        else:
            break
        file.readline()
    print("-" * 20)
    print("A taxa de acerto foi de: ", (100 * acertos / (acertos + erros)), "%")
    print("-" * 20)


if __name__ == '__main__':
    phrases = open("/home/ricardo/Documents/Samples/Evaluations/frases_dialogflow.txt", mode="r")
    dialogflow_evaluation(phrases)
