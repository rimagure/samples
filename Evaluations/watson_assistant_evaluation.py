from __future__ import print_function
from watson_developer_cloud import AssistantV1


def detect_intent_texts(assistant, workspace_id, input_text):
    response = assistant.message(
        workspace_id=workspace_id,
        input={
            'text': input_text
        }).get_result()
    #print(response['intents'][0]['intent'])
    return response['intents'][0]['intent'], response['intents'][0]['confidence']


def watson_evaluation(file):
    assistant = AssistantV1(
        username='apikey',
        password='423ehYFN--SbgEcuefvrF-o0j9AakZfwHEzTntpuuxhx',
        ## url is optional, and defaults to the URL below. Use the correct URL for your region.
        url='https://gateway.watsonplatform.net/assistant/api',
        version='2018-09-20')

    workspace_id = '338785a0-b933-4641-9b12-6a5f1018ac4a'
    print('Workspace id {0}'.format(workspace_id))
    acertos = 0
    erros = 0
    while 1:
        phrase = file.readline()
        phrase = phrase.strip()
        intent = file.readline()
        intent = intent.strip()
        if phrase:
            detected_intent, confidence = detect_intent_texts(assistant, workspace_id, phrase)
            if intent == detected_intent:
                acertos += 1
                print("-" * 20)
                print("[Acerto: %s]" % intent)
                print("A frase foi: ", phrase)
                print("Era para ser: ", intent)
                print("E foi: ", detected_intent)
                print("Confidence: ", confidence)

            else:
                erros += 1
                print("-" * 20)
                print("[Erro: %s]" %intent)
                print("A frase foi: ", phrase)
                print("Era para ser: ", intent)
                print("Mas foi: ", detected_intent)
                print("Confidence: ", confidence)
        else:
            break
        file.readline()
    print("-" * 20)
    print("A taxa de acerto foi de: ", (100 * acertos / (acertos + erros)), "%")
    print("-" * 20)


if __name__ == '__main__':
    phrases = open("/home/ricardo/Documents/Samples/Evaluations/frases_dialogflow.txt", mode="r")
    watson_evaluation(phrases)
