import spacy


def spacy_evaluation(file):
    nlp = spacy.load('pt_core_news_sm')
    acertos = 0
    erros = 0
    while 1:
        line = file.readline()
        if line:
            print("")
            #print("")
            print(line, end="")
            doc = nlp(line)
            line2 = file.readline()
            for token, word in zip(doc, line2.split()):
                if token.pos_ != 'SPACE' and token.pos_ != 'PUNCT':
                    if token.pos_ == word:
                        print("Correto (O token é: %s):" % token, end=" ")
                        acertos += 1
                    else:
                        print("Erro (O token é: %s):" % token, end=" ")
                        erros += 1
                    print(token.pos_, end=" ")
                    print(word)
        else:
            break
        file.readline()
    print("-"*20)
    print("A taxa de acerto foi de: ", (100*acertos/(acertos+erros)), "%")
    print("-"*20)


if __name__ == '__main__':
    phrases = open("/home/ricardo/Documents/Samples/Evaluations/frases_spacy.txt", mode="r") #trabalho
    #file = open("/home/ricardo/Documents/Projects/samples/Evaluations/frases_spacy.txt", mode="r")  # casa
    spacy_evaluation(phrases)
