from NLPs_tests.StanfordCore_NLP_test import *


def stanfordnlp_evaluation(file):
    sNLP = StanfordNLP()
    acertos = 0
    erros = 0
    while 1:
        line = file.readline()
        if line:
            print("")
            #print("")
            print(line, end="")
            doc = sNLP.pos(line)
            line0 = []
            line1 = []
            line2 = file.readline()
            for item in doc:
                line0.append(item[0])
                line1.append(item[1])
            for word, pos, pos_c in zip(line0, line1, line2.split()):
                #print(pos, end=" ")
                if pos == pos_c:
                    print("Correto (O token é: %s):" % word, end=" ")
                    acertos += 1
                else:
                    print("Erro (O token é: %s):" % word, end=" ")
                    erros += 1
                print(pos, end=" ")
                print(pos_c)
        else:
            break
        file.readline()
    print("")
    print("-"*20)
    print("A taxa de acerto foi de: ", (100*acertos/(acertos+erros)), "%")
    print("-"*20)


if __name__ == '__main__':
    phrases = open("/home/ricardo/Documents/Samples/Evaluations/frases_stanford.txt", mode="r") #trabalho
    #phrases = open("/home/ricardo/Documents/Projects/samples/Evaluations/frases_stanford.txt", mode="r")  # casa
    stanfordnlp_evaluation(phrases)
